#ifndef __BSP_MQTT_H__
#define __BSP_MQTT_H__

#include "Common.h"

//用户需要根据设备信息完善以下宏定义中的四元组内容
#define PRODUCT_KEY    	"geotXdPfuto"							//阿里云颁发的产品唯一标识，11位长度的英文数字随机组合
#define DEVICE_NAME    	"device1"								//用户注册设备时生成的设备唯一编号，支持系统自动生成，也可支持用户添加自定义编号，产品维度内唯一
#define DEVICE_SECRET  	"f04f4367fb9c7ae6c7494c5c7725418e"		//设备密钥，与DeviceName成对出现，可用于一机一密的认证方案

//#define PRODUCT_SECRET 	"a1L5lKy2Cpn"						//阿里云颁发的产品加密密钥，通常与ProductKey成对出现，可用于一型一密的认证方案


//以下宏定义固定，不需要修改
#define HOST_NAME  			"iot-06z00fz0kdevbyh.mqtt.iothub.aliyuncs.com"	//阿里云域名
#define HOST_PORT 			1883																																						//阿里云域名端口，固定1883
#define CONTENT				"clientId"DEVICE_NAME"deviceName"DEVICE_NAME"productKey"PRODUCT_KEY	//计算登录密码用
#define CLIENT_ID			DEVICE_NAME"|securemode=3,signmethod=hmacsha1|"						//客户端ID
#define USER_NAME			DEVICE_NAME"&"PRODUCT_KEY																													//客户端用户名
//#define PASSWORD			"5444B6A2881B52D4A9A44188FD8F1397B3A37046"		//客户端登录password通过hmac_sha1算法得到，大小写不敏感
#define DEVICE_PUBLISH		"/"PRODUCT_KEY"/"DEVICE_NAME"/user/update"		//发布topic
#define DEVICE_SUBSCRIBE	"/"PRODUCT_KEY"/"DEVICE_NAME"/user/get"			//订阅topic

//以下三个TOPIC的宏定义不需要用户修改，可以直接使用
//IOT HUB为设备建立三个TOPIC：update用于设备发布消息，error用于设备发布错误，get用于订阅消息
//#define TOPIC_UPDATE         "/"PRODUCT_KEY"/"DEVICE_NAME"/update"
//#define TOPIC_ERROR          "/"PRODUCT_KEY"/"DEVICE_NAME"/update/error"
//#define TOPIC_GET            "/"PRODUCT_KEY"/"DEVICE_NAME"/get"

void getPassword(const char *device_secret, const char *content, char *password);
void bsp_mqtt_init(void);
void bsp_mqtt_publish(char *data);


#endif


