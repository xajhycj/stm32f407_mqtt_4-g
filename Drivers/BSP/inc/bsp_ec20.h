#ifndef __BSP_EC20_H__
#define __BSP_EC20_H__

#include "Common.h"

#define AT_QMTCFG_VERSION   "AT+QMTCFG=\"version\",0,4\r\n"
#define AT_QMTCFG_ALIYUN	"AT+QMTCFG=\"aliauth\",0,\"%s\",\"%s\",\"%s\"\r\n"
#define AT_QMTOPEN			"AT+QMTOPEN=0,\"%s\",%d\r\n"
#define AT_QMTCONN			"AT+QMTCONN=0,\"%s\",\"%s\",\"%s\"\r\n"
#define AT_QMTPUBEX			"AT+QMTPUBEX=0,%d,%d,%d,\"%s\",%d\r\n"
#define AT_QMTSUB			"AT+QMTSUB=0,%d,\"%s\",%d\r\n"


typedef enum
{
  POWER_OFF = 0,
  POWER_ON
}POWER;


void bsp_ec20_init(void);
int32_t ec20_at_mqtt_init(char *ProductKey, char *DeviceName, char *DeviceSecret);
int32_t ec20_at_mqtt_open(char *host, int port);
int32_t ec20_at_mqtt_connect(char *client_id, char *username, char *password);
int32_t ec20_at_mqtt_publish(char *topic, int msgid, int qos, int retain, char *data, int len);
int32_t ec20_at_mqtt_sublish(char *topic, int msgid, int qos);
int32_t bsp_ec20_gettime(void);

#endif

