#include "bsp_time.h"
#include "bsp_ec20.h"
#include "drv_time.h"

#if 0
struct tm {
int tm_sec; /* 秒 – 取值区间为[0,59] */
int tm_min; /* 分 - 取值区间为[0,59] */
int tm_hour; /* 时 - 取值区间为[0,23] */
int tm_mday; /* 一个月中的日期 - 取值区间为[1,31] */
int tm_mon; /* 月份（从一月开始，0代表一月） - 取值区间为[0,11] */
int tm_year; /* 年份，其值等于实际年份减去1900 */
int tm_wday; /* 星期 – 取值区间为[0,6]，其中0代表星期天，1代表星期一，以此类推 */
int tm_yday; /* 从每年的1月1日开始的天数 – 取值区间为[0,365]，其中0代表1月1日，1代表1月2日，以此类推 */
int tm_isdst; /* 夏令时标识符，实行夏令时的时候，tm_isdst为正。不实行夏令时的时候，tm_isdst为0；不了解情况时，tm_isdst()为负。*/
long int tm_gmtoff; /*指定了日期变更线东面时区中UTC东部时区正秒数或UTC西部时区的负秒数*/
const char *tm_zone; /*当前时区的名字(与环境变量TZ有关)*/
};

typedef long  time_t;    /* 时间值time_t 为长整型的别名*/

+CCLK: "21/10/07,05:26:18+00"

#endif

void set_time(struct tm st, int timezone)
{
	time_t tt;
	uint64 ticks;
	tt=mktime(&st);
	//tt = tt + (8-timezone)*3600;
	ticks = (uint64)tt*1000;
	my_setTick(ticks);
	printf("tick = %ld, timezone=%d\n",tt, timezone);
}


//16分钟差1秒，10/20分钟对一次时
void Timesync(void)
{
	bsp_ec20_gettime();
}

