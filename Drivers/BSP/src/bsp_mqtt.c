#include "bsp_mqtt.h"
#include "hmac.h"
#include "bsp_ec20.h"
/*
// C prototype : void HexToStr(BYTE *pbDest, BYTE *pbSrc, int nLen)
// parameter(s): [OUT] pbDest - 存放目标字符串
// [IN] pbSrc - 输入16进制数的起始地址
// [IN] nLen - 16进制数的字节数
// return value: 
// remarks : 将16进制数转化为字符串
*/
void HexToStr(uint8_t *pbDest, uint8_t *pbSrc, int nLen)
{
	char ddl,ddh;
	int i;

	for (i=0; i<nLen; i++)
	{
		ddh = 48 + pbSrc[i] / 16;
		ddl = 48 + pbSrc[i] % 16;
		if (ddh > 57) ddh = ddh + 7;
		if (ddl > 57) ddl = ddl + 7;
		pbDest[i*2] = ddh;
		pbDest[i*2+1] = ddl;
	}

	pbDest[nLen*2] = '\0';
}

//通过hmac_sha1算法获取password
void getPassword(const char *device_secret, const char *content, char *password)
{
	char buf[256] = {0};
	int len = sizeof(buf);

	hmac_sha1(device_secret, strlen(device_secret), content, strlen(content), buf, &len);
	HexToStr(password, buf, len);
}

void bsp_mqtt_init(void)
{
	char PASSWORD[200] = {0};
	getPassword(DEVICE_SECRET, CONTENT, PASSWORD);			//通过hmac_sha1算法得到password
	//printf("PassWord = %s\r\n", PASSWORD);
	ec20_at_mqtt_init(PRODUCT_KEY, DEVICE_NAME, DEVICE_SECRET);
	ec20_at_mqtt_open(HOST_NAME, HOST_PORT);
	ec20_at_mqtt_connect(DEVICE_NAME, USER_NAME, PASSWORD);
	ec20_at_mqtt_sublish(DEVICE_SUBSCRIBE, 12, 0);
}

void bsp_mqtt_publish(char *data)
{
	//int len = strlen(data);
	ec20_at_mqtt_publish(DEVICE_PUBLISH, 0, 0, 0, data, strlen(data));
}




