#include "drv_at.h"
#include "bsp_ec20.h"
#include "bsp_mqtt.h"
#include "bsp_time.h"

int32_t cesq_rsp_fun(char *rsp, char *delay_rsp)
{//+CESQ:42,99,255,255,34,51
	int32_t res = STATE_SUCCESS;
	char *strx = NULL;
	int csq = 0;
	strx=strstr((const char*)rsp,(const char*)"+CESQ");//返回CSQ
	if(strx){
		csq=(strx[6]-0x30)*10+(strx[7]-0x30);//获取CSQ
		printf("cesq:%d\r\n", csq);
		if((csq==99)||((strx[6]-0x30)==0)){ //说明信号质量差，扫网失败
			res = STATE_AT_CESQ_FAILED;
		}
	}else{
		res = STATE_AT_CESQ_FAILED;
	}
	return res;
}

int32_t at_rsp_delay_handler(char *rsp, char *delay_rsp)
{
    delayms(200);
    return STATE_SUCCESS;
}

int32_t at_cclk_handler(char *rsp, char *delay_rsp)
{//+CCLK: "21/10/07,05:48:22+00"
	int32_t res = STATE_SUCCESS;
    struct tm st;
	char *strx = NULL;
	int tmp;
	int timezone;
	strx=strstr((const char*)rsp,(const char*)"+CCLK");
	if(strx){
		tmp=(strx[8]-0x30)*10+(strx[9]-0x30);  //年
		st.tm_year=100+tmp; //year=1900+121=2021
		tmp=(strx[11]-0x30)*10+(strx[12]-0x30);//月
		st.tm_mon=tmp-1; //[0-11]
		tmp=(strx[14]-0x30)*10+(strx[15]-0x30);//日
		st.tm_mday=tmp; //[1,31]
		tmp=(strx[17]-0x30)*10+(strx[18]-0x30);//时
		st.tm_hour=tmp; //[0,23]
		tmp=(strx[20]-0x30)*10+(strx[21]-0x30);//分
		st.tm_min=tmp; //[0,59]
		tmp=(strx[23]-0x30)*10+(strx[24]-0x30);//秒
		st.tm_sec=tmp; //[0,59]
		timezone = (strx[26]-0x30)*10+(strx[27]-0x30);//时区
		set_time(st, timezone);
		
	}else{
		res = STATE_AT_CCLK_FAILED;
	}
    return res;
}



/* 模块初始化命令列表 */
static const at_cmd_item_t at_bootstrap_cmd_list[] = {
    {   /* UART通道测试 */
        .cmd = "AT\r\n",
        .cmd_len = sizeof("AT\r\n") - 1,
        .rsp = "OK",
        .handler = NULL,
    },
    {   /* 关闭回显 */
        .cmd = "ATE0\r\n",
        .cmd_len = sizeof("ATE0\r\n") - 1,
        .rsp = "OK",
        .handler = NULL,
    },
    {   /* 获取通信模块 IMEI 号 */
        .cmd = "AT+CGSN\r\n",
        .cmd_len = sizeof("AT+CGSN\r\n") - 1,
        .rsp = "OK",
        .handler = NULL,
    },
    {   /* 获取模组固件版本号 */
        .cmd = "AT+CGMR\r\n",
        .cmd_len = sizeof("AT+CGMR\r\n") - 1,
        .rsp = "OK",
        .handler = NULL,
    },
    {   /* 检查SIM卡 */
        .cmd = "AT+CPIN?\r\n",
        .cmd_len = sizeof("AT+CPIN?\r\n") - 1,
        .rsp = "READY",
        .handler = NULL,
    },
    {   /* 获取SIM卡IMSI */
        .cmd = "AT+CIMI\r\n",
        .cmd_len = sizeof("AT+CIMI\r\n") - 1,
        .rsp = "OK",
        .handler = NULL,
    },
    {   /* 检查信号强度 */
        .cmd = "AT+CESQ\r\n",
        .cmd_len = sizeof("AT+CESQ\r\n") - 1,
        .rsp = "+CESQ:",
        .handler = cesq_rsp_fun,
    },
};

void bsp_ec20_power(POWER power)
{
	static POWER power_status = POWER_OFF;
	if(power == POWER_ON && power_status == POWER_OFF)
	{
		HAL_GPIO_WritePin(EC20_PWRKEY_GPIO_Port, EC20_PWRKEY_Pin, GPIO_PIN_RESET);
		delayms(600);
		HAL_GPIO_WritePin(EC20_PWRKEY_GPIO_Port, EC20_PWRKEY_Pin, GPIO_PIN_SET);
		delayms(5000);
		power_status = POWER_ON;
	}
	else if(power == POWER_OFF && power_status == POWER_ON)
	{
		HAL_GPIO_WritePin(EC20_PWRKEY_GPIO_Port, EC20_PWRKEY_Pin, GPIO_PIN_RESET);
		delayms(600);
		HAL_GPIO_WritePin(EC20_PWRKEY_GPIO_Port, EC20_PWRKEY_Pin, GPIO_PIN_SET);
		delayms(2000);
		power_status = POWER_OFF;
	}
}

void bsp_ec20_repower(void)
{
	bsp_ec20_power(POWER_OFF);
	bsp_ec20_power(POWER_ON);
}

void bsp_ec20_init(void)
{
	int32_t res;
	bsp_ec20_repower();
	do{
		res = drv_at_commands_send(at_bootstrap_cmd_list,
	                               sizeof(at_bootstrap_cmd_list) / sizeof(at_bootstrap_cmd_list[0]));
		if(res != STATE_SUCCESS){
			printf("ec20 init fail -0x%x\r\n", -res);
		}else{
			printf("ec20 init ok\r\n");
		}
	}while(res != STATE_SUCCESS);
}

int32_t ec20_at_mqtt_init(char *ProductKey, char *DeviceName, char *DeviceSecret)
{
	char cmd[AIOT_AT_CMD_LEN_MAXIMUM] = {0};
	
	//memset(cmd, 0, sizeof(cmd));
    snprintf(cmd, sizeof(cmd), AT_QMTCFG_ALIYUN, ProductKey, DeviceName, DeviceSecret);
	
	at_cmd_item_t at_mqtt_cmd_list[] = {
	    {   /* version 3.1.1 */
	        .cmd = AT_QMTCFG_VERSION,
	        .cmd_len = strlen(AT_QMTCFG_VERSION),
	        .rsp = "OK",
	        .handler = NULL,
	    },
	     {   /* aliyun */
	        .cmd = cmd,
	        .cmd_len = strlen(cmd),
	        .rsp = "OK",
	        .handler = NULL,
	    },
	};

	return drv_at_commands_send(at_mqtt_cmd_list,
	                     sizeof(at_mqtt_cmd_list) / sizeof(at_mqtt_cmd_list[0]));
}

int32_t ec20_at_mqtt_open(char *host, int port)
{
	char cmd[AIOT_AT_CMD_LEN_MAXIMUM] = {0};
	
    snprintf(cmd, sizeof(cmd), AT_QMTOPEN, host, port);
	
	at_cmd_item_t at_mqtt_cmd_list[] = {
	     {
	        .cmd = cmd,
	        .cmd_len = strlen(cmd),
	        .rsp = "OK",
	        .delay_rsp = "+QMTOPEN: 0,0",
	        .handler = NULL,
	    },
	};

	return drv_at_commands_send(at_mqtt_cmd_list,
	                     sizeof(at_mqtt_cmd_list) / sizeof(at_mqtt_cmd_list[0]));
}

int32_t ec20_at_mqtt_connect(char *client_id, char *username, char *password)
{
	char cmd[AIOT_AT_CMD_LEN_MAXIMUM] = {0};
	
    snprintf(cmd, sizeof(cmd), AT_QMTCONN, client_id, username, password);
	
	at_cmd_item_t at_mqtt_cmd_list[] = {
	     {
	        .cmd = cmd,
	        .cmd_len = strlen(cmd),
	        .rsp = "OK",
	        .delay_rsp = "+QMTCONN: 0,0,0",
	        .handler = NULL,
	    },
	};

	return drv_at_commands_send(at_mqtt_cmd_list,
	                     sizeof(at_mqtt_cmd_list) / sizeof(at_mqtt_cmd_list[0]));
}

int32_t ec20_at_mqtt_publish(char *topic, int msgid, int qos, int retain, char *data, int len)
{
	char cmd[AIOT_AT_CMD_LEN_MAXIMUM] = {0};

    snprintf(cmd, sizeof(cmd), AT_QMTPUBEX, msgid, qos, retain, topic, len);
	
	at_cmd_item_t at_mqtt_cmd_list[] = {
	     {
	        .cmd = cmd,
	        .cmd_len = strlen(cmd),
	        .rsp = ">",
	        .handler = at_rsp_delay_handler,
	    },
	    {
	        .cmd = data,
	        .cmd_len = len,
	        .rsp = "OK",
	        .handler = at_rsp_delay_handler,
	    },
	};

	return drv_at_commands_send(at_mqtt_cmd_list,
	                     sizeof(at_mqtt_cmd_list) / sizeof(at_mqtt_cmd_list[0]));
}

int32_t ec20_at_mqtt_sublish(char *topic, int msgid, int qos)
{
	char cmd[AIOT_AT_CMD_LEN_MAXIMUM] = {0};

    snprintf(cmd, sizeof(cmd), AT_QMTSUB, msgid, topic, qos);
	
	at_cmd_item_t at_mqtt_cmd_list[] = {
	     {
	        .cmd = cmd,
	        .cmd_len = strlen(cmd),
	        .rsp = "OK",
	        .delay_rsp = "+QMTSUB: 0",
	        .handler = at_rsp_delay_handler,
	    },
	};

	return drv_at_commands_send(at_mqtt_cmd_list,
	                     sizeof(at_mqtt_cmd_list) / sizeof(at_mqtt_cmd_list[0]));
}

int32_t bsp_ec20_gettime(void)
{
	at_cmd_item_t at_mqtt_cmd_list[] = {
	     {
	        .cmd = "at+cclk?\r\n",
	        .cmd_len = strlen("at+cclk?\r\n"),
	        .rsp = "OK",
	        .handler = at_cclk_handler,
	    },
	};

	return drv_at_commands_send(at_mqtt_cmd_list,
	                     sizeof(at_mqtt_cmd_list) / sizeof(at_mqtt_cmd_list[0]));
}


