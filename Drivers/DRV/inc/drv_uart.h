#ifndef __DRV_UART_H__
#define __DRV_UART_H__

#include "Common.h"

int drv_uart1_Printf(const char *fmt, ...);
int drv_uart3_Printf(const char *fmt, ...);

#define INF(format,...) drv_uart1_Printf(format, ##__VA_ARGS__)

void drv_uart_user_init(void);
int drv_uart1_send_buffer(uint8_t *buffer, int length);
int drv_uart3_send_buffer(uint8_t *buffer, int length);
void uart_process(void);

#endif

