#ifndef __DRV_LED_H__
#define __DRV_LED_H__

#include "Common.h"

void drv_led_on_all(void);
void drv_led_off_all(void);
void drv_led_trigger_all(void);
void drv_led_trigger(uint8_t led);


#endif

