#ifndef __DRV_AT_H__
#define __DRV_AT_H__

#include "Common.h"

#define MODULE_NAME_AT                      "AT"

/**
 * @brief API执行成功
 *
 */
#define STATE_SUCCESS                                               (0x0000)

/**
 * @brief -0x0100~-0x01FF表达SDK检查用户输入参数时反馈的状态码
 *
 */
#define STATE_USER_INPUT_BASE                                       (-0x0100)

/**
 * @brief 用户输入参数中包含非法的空指针
 *
 */
#define STATE_USER_INPUT_NULL_POINTER                               (-0x0101)



/**
 * @brief -0x1000~-0x10FF表达SDK在AT模块内的状态码
 */
#define STATE_AT_BASE                       (-0x1000)
#define STATE_AT_ALREADY_INITED             (-0x1003)
#define STATE_AT_NOT_INITED                 (-0x1004)
#define STATE_AT_UART_TX_FUNC_MISSING       (-0x1005)
#define STATE_AT_GET_RSP_FAILED             (-0x1006)
#define STATE_AT_RINGBUF_OVERFLOW           (-0x1007)
#define STATE_AT_RX_TIMEOUT                 (-0x1008)
#define STATE_AT_NO_AVAILABLE_LINK          (-0x1009)
#define STATE_AT_TX_TIMEOUT                 (-0x100A)
#define STATE_AT_TX_ERROR                   (-0x100B)
#define STATE_AT_RINGBUF_NO_DATA            (-0x100C)
#define STATE_AT_UART_TX_FUNC_NULL          (-0x100D)
#define STATE_AT_NO_DATA_SLOT               (-0x100F)
#define STATE_AT_LINK_IS_DISCONN            (-0x1010)
#define STATE_AT_UART_TX_FAILED             (-0x1011)
#define STATE_AT_UART_RX_FAILED             (-0x1012)
#define STATE_AT_CESQ_FAILED             	(-0x1013)
#define STATE_AT_CCLK_FAILED             	(-0x1014)


/**
 * @brief AT组件配置参数
 */
#define AIOT_AT_CMD_LEN_MAXIMUM             (128)       /* AT命令最大长度 */
#define AIOT_AT_RSP_LEN_MAXIMUM             (64)        /* AT应答最大长度 */
#define AIOT_AT_TX_TIMEOUT_DEFAULT          (5000)      /* UART默认发送超时时间 */
#define AIOT_AT_RX_TIMEOUT_DEFAULT          (5000)      /* UART默认接受等待超时时间 */
#define AIOT_AT_DATA_RB_SIZE_DEFAULT        (1200)      /* 内部网络数据接收缓冲区大小 */
#define AIOT_AT_RSP_RB_SIZE_DEFAULT         (200)        /* 内部应答报文接受缓冲区大小 */
#define AIOT_AT_CMD_RETRY_TIME              (3)         /* AT命令发送重试最大次数 */
#define AIOT_AT_RINGBUF_RETRY_INTERVAL      (20)       /* RINGBUF数据长度检查间隔时间 */
#define AIOT_AT_SOCKET_NUM                  (5)         /* 支持的数据链路数量 */



/**
 * @brief 串口发送回调函数定义
 *
 * @param[in] p_data
 * @param[in] len
 * @param[in] timeout
 *
 * @return int32_t
 * @retval length, 实际发送的字节数
 * @retval 0, 发送超时
 * @retval -1, 发送失败
 */
typedef int32_t (*aiot_at_uart_tx_func_t)(const uint8_t *p_data, uint16_t len, uint32_t timeout);

/**
 * @brief 接受到应答数据后的用户处理回调函数原型定义
 *
 * @param[in] rsp       AT命令的应答数据
 * @param[in] user_data 用户自定义数据
 *
 * @return int32_t
 * @retval >= 0,
 * @retval < 0,
 */
typedef int32_t (*at_rsp_handler_t)(char *rsp, char *delay_rsp);

/**
 * @brief AT命令请求响应结构体定义
 */
typedef struct {
    /* 要发送的AT命令 */
    char *cmd;
    /* AT命令的数据长度 */
    uint32_t cmd_len;
    /* 期望的应答数据 */
    char *rsp;
    /* 接受到应答数据后的用户处理回调函数 */
    at_rsp_handler_t handler;
    /* has deleyed response */
    char *delay_rsp;
    /* has expected len*/
    uint32_t expected_len;
} at_cmd_item_t;

/**
 * @brief ringbuf结构体定义
 */
typedef struct {
    uint8_t *buf;
    uint8_t *head;
    uint8_t *tail;
    uint8_t *end;
    uint32_t size;
    SemaphoreHandle_t mutex;
}ringbuf_t;


/**
 * @brief AT组件上下文数据结构体
 */
typedef struct {
    uint8_t is_init;
    uint32_t tx_timeout;
    uint32_t rsp_timeout;

   // core_at_parse_sm_t sm;
    ringbuf_t rsp_rb;
    char rsp_buf[AIOT_AT_RSP_LEN_MAXIMUM];
    uint32_t rsp_buf_offset;

    aiot_at_uart_tx_func_t uart_tx_func;
    SemaphoreHandle_t tx_mutex;

    void *user_data;
} at_handle_t;


int32_t drv_at_init(void);
int32_t drv_ringbuf_write(const uint8_t *data, uint32_t len);
int32_t drv_at_commands_send(const at_cmd_item_t *cmd_list, uint16_t cmd_num);


#endif


