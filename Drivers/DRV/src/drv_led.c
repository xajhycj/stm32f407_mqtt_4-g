#include "drv_led.h"

void drv_led_on_all(void)
{
	 HAL_GPIO_WritePin(GPIOB, LED1_Pin|LED2_Pin|LED3_Pin|LED4_Pin, GPIO_PIN_RESET);
}

void drv_led_off_all(void)
{
	 HAL_GPIO_WritePin(GPIOB, LED1_Pin|LED2_Pin|LED3_Pin|LED4_Pin, GPIO_PIN_SET);
}

void drv_led_trigger_all(void)
{
	 HAL_GPIO_TogglePin(GPIOB, LED1_Pin|LED2_Pin|LED3_Pin|LED4_Pin);
}

void drv_led_trigger(uint8_t led)
{
	switch(led){
		case 1:
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
			break;
		case 2:
			HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
			break;
		case 3:
			HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
			break;
		case 4:
			HAL_GPIO_TogglePin(LED4_GPIO_Port, LED4_Pin);
			break;
		default:
			break;
	}
	
}


