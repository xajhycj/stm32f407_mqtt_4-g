#include "driver.h"

void drv_init(void)
{
	drv_led_off_all();
	drv_uart_user_init();
	drv_at_init();
}
