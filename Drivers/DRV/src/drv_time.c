#include "drv_time.h"

__IO uint64 myTick;
HAL_TickFreqTypeDef myTickFreq = HAL_TICK_FREQ_DEFAULT;  /* 1KHz */

void my_IncTick(void)
{
  myTick += myTickFreq;
}

uint64_t my_GetTick(void)
{
  return myTick;
}

void my_setTick(uint64 tick)
{
   myTick = tick;
}


uint64 get_timestamp(void)
{
	return (uint64)(my_GetTick() * portTICK_RATE_MS);
}


