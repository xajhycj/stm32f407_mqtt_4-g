#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "Common.h"

#define DEV_FACTORY_LEN 8
#define DEV_TYPE_LEN 	8
#define DEV_SN_LEN		20

/*
sizeof(stDevConfig)=36
sizeof(stDevParam)=16
sizeof(stSensorParam)=24
*/
typedef struct
{
	uint8_t device_factory[DEV_FACTORY_LEN];	//设备厂家
	uint8_t device_type[DEV_TYPE_LEN];			//设备类型
	uint8_t device_sn[DEV_SN_LEN];				//设备SN
}stDevConfig;

typedef struct
{
	int32_t device_longitude;	//经度    待确定经度和范围
	int32_t device_latitude;	//纬度
	int32_t device_altitude;	//海拔
	int16_t device_comselect;	//输出接口选择，RJ45 4G等
	int16_t device_id;			//modbus的ID号
}stDevParam;

/*
E=(v-v0)/(k*kw)
E:被测电场值
v:传感器输出原始值
v0:传感器零点标定值
k:传感器灵敏度系数
kw:室外标定系数
*/
typedef struct
{
	int32_t mems_v0;	//传感器零点标定值
	int32_t mems_k;		//传感器灵敏度系数
	int32_t mems_kw;	//室外标定系数
	int32_t mems_e1;	//1级预警阈值
	int32_t mems_e2;	//2级预警阈值
	int32_t mems_e3;	//3级预警阈值
}stSensorParam;

void config_test(void);

#endif


