#include"config.h"

stDevConfig gDevConfig;
stDevParam gDevParam;
stSensorParam gSensorParam;

void set_dev_config(stDevConfig *pdev_config)
{
	memcpy((uint8_t *)&gDevConfig, (uint8_t *)pdev_config, sizeof(stDevConfig));
}

void set_dev_param(stDevParam *pdev_param)
{
	memcpy((uint8_t *)&gDevParam, (uint8_t *)pdev_param, sizeof(stDevParam));
}

void set_sensor_param(stSensorParam *psensor_param)
{
	memcpy((uint8_t *)&gSensorParam, (uint8_t *)psensor_param, sizeof(stSensorParam));
}

void config_test(void)
{
	printf("sizeof(stDevConfig)=%d\n", sizeof(stDevConfig));
	printf("sizeof(stDevParam)=%d\n", sizeof(stDevParam));
	printf("sizeof(stSensorParam)=%d\n", sizeof(stSensorParam));
}

