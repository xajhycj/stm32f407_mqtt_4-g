#include "my_task.h"
#include "task_led.h"
#include "task_at.h"
#include "driver.h"

__IO bool os_start_flag = false;

void set_os_start_flag(bool flag)
{
	os_start_flag = flag;
}

bool get_os_start_flag(void)
{
	return os_start_flag;
}

void my_task_init(void)
{
	if(led_task_init() == pdPASS){
		printf("led task init ok\r\n");
	}else{
		printf("led task init fail\r\n");
	}

	if(at_task_init() == pdPASS){
		printf("at task init ok\r\n");
	}else{
		printf("at task init fail\r\n");
	}
}


