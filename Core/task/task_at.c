#include "task_at.h"
#include "driver.h"
#include "bsp_ec20.h"
#include "bsp_mqtt.h"

#define AT_TASK_PRIO		osPriorityNormal
#define AT_STK_SIZE 		(1024)
TaskHandle_t AtTask_Handler;

void at_task(void *arg)
{
//	bsp_ec20_init();
	//bsp_mqtt_init();
//	Timesync();
	delayms(2000);
	while(1)
	{
		//uart_process();
		//bsp_mqtt_publish("hello world");
		drv_led_trigger(1);
		delayms(100);
	}
}

BaseType_t at_task_init(void)
{
   return xTaskCreate((TaskFunction_t )at_task,            //任务函数
                (const char*    )"at_task",          //任务名称
                (uint16_t       )AT_STK_SIZE,        //任务堆栈大小
                (void*          )NULL,                  //传递给任务函数的参数
                (UBaseType_t    )AT_TASK_PRIO, 		//任务优先级
                (TaskHandle_t*  )&AtTask_Handler);   //任务句柄              
}


