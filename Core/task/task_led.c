#include "task_led.h"
#include "driver.h"
#include "my_task.h"
#include "config.h"


#define LED_TASK_PRIO		osPriorityAboveNormal
#define LED_STK_SIZE 		(1024)
TaskHandle_t LedTask_Handler;

void led_task(void *arg)
{
	set_os_start_flag(true);
	delayms(1000);
	config_test();
	while(1)
	{
		drv_led_trigger(4);
		//printf("time=%lld\r\n", get_timestamp());
		delayms(1000);
		
	}
}

BaseType_t led_task_init(void)
{
   return xTaskCreate((TaskFunction_t )led_task,            //任务函数
                (const char*    )"led_task",          //任务名称
                (uint16_t       )LED_STK_SIZE,        //任务堆栈大小
                (void*          )NULL,                  //传递给任务函数的参数
                (UBaseType_t    )LED_TASK_PRIO, 		//任务优先级
                (TaskHandle_t*  )&LedTask_Handler);   //任务句柄              
}

