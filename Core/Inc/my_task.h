#ifndef __MY_TASK_H__
#define __MY_TASK_H__

#include "Common.h"

void set_os_start_flag(bool flag);
bool get_os_start_flag(void);

void my_task_init(void);

#endif

