/* std_basal.h - 规范化基本数据类型定义头文件 */
/* 版权归属Geek+ */
/* 2015.10.20 */

#ifndef STD_MACRO_FUN_EXP_H
#define STD_MACRO_FUN_EXP_H

#include "std_basal.h"

/* CONVENIENT_FOR_LOOKUP
void ui32_mask(){} */
/* ----------------------------------------------------------------------------------------- */
/* 32位掩码 */
#define MASK_ALL8F                      0xFFFFFFFF

/* 32位中的高24位 */
#define MASK_HI24                       0xFFFFFF00

/* 32位中的高16位 */
#define MASK_HI16                       0xFFFF0000

/* 32位中的高8位 */
#define MASK_HI08                       0xFF000000

/* 32位中的23-16位 */
#define MASK_23_16                      0x00FF0000

/* 32位中的15-8位 */
#define MASK_15_08                      0x0000FF00

/* 32位中的低24位 */
#define MASK_LO24                       0x00FFFFFF

/* 32位中的低16位 */
#define MASK_LO16                       0x0000FFFF

/* 32位中的低8位 */
#define MASK_LO08                       0x000000FF

/* 32位中的低4位 */
#define MASK_LO04                       0x0000000F

#define MASK_BITH4                      0xF0        /* B7,B6,B5,B4位掩码 */
#define MASK_BITL4                      0x0F        /* B3,B2,B1,B0位掩码 */
#define MASK_BIT7                       0x80        /* B7位掩码 */
#define MASK_BIT6                       0x40        /* B6位掩码 */
#define MASK_BIT5                       0x20        /* B5位掩码 */
#define MASK_BIT4                       0x10        /* B4位掩码 */
#define MASK_BIT3                       0x08        /* B3位掩码 */
#define MASK_BIT2                       0x04        /* B2位掩码 */
#define MASK_BIT1                       0x02        /* B1位掩码 */
#define MASK_BIT0                       0x01        /* B0位掩码 */

/* CONVENIENT_FOR_LOOKUP
void ui_byte_operation(){} */
/* ----------------------------------------------------------------------------------------- */
/* 提取32位无符号整型数的四个字节数,big indian */
#define UI32_HIHI8(ui32)                ((unint08)(((ui32) >> 24) & MASK_LO08))
#define UI32_HILO8(ui32)                ((unint08)(((ui32) >> 16) & MASK_LO08))
#define UI32_LOHI8(ui32)                ((unint08)(((ui32) >>  8) & MASK_LO08))
#define UI32_LOLO8(ui32)                ((unint08)(((ui32)      ) & MASK_LO08))

/* 提取32位float raw 4 bytes data */
#define FLT32_TO_MEM(fptr)          (*((volatile unint32 *)(fptr)))
 
/* 提取32位无符号整型数的两个短整型数,big indian */
#define UI32_HI16(ui32)                 ((unint16)(((ui32) >> 16) & MASK_LO16))
#define UI32_LO16(ui32)                 ((unint16)(((ui32)      ) & MASK_LO16))

/* 提取16位无符号整型数的两个字节数,big indian */
#define UI16_HI8(ui16)                  ((unint08)(((ui16) >>  8) & MASK_LO08))
#define UI16_LO8(ui16)                  ((unint08)(((ui16)      ) & MASK_LO08))

/* 提取8位无符号整型数的4bit数,big indian */
#define UI8_HI4(ui8)                    (((ui8) >> 4) & MASK_LO04)
#define UI8_LO4(ui8)                    (((ui8)     ) & MASK_LO04)

/* 将8个字节数拼成64位无符号整型数,big indian */
#define UI64_MAKE16(hh, hl, lh, ll)     ((((ulong64)(hh)) << 48) | (((ulong64)(hl)) << 32) | (((ulong64)(lh)) << 16) | ((ll)))

/* 将四个字节数拼成32位无符号整型数,big indian */
#define UI32_MAKE(hh8, hl8, lh8, ll8)   ((((unint32)(hh8)) << 24) | (((unint32)(hl8)) << 16) | (((unint32)(lh8)) << 8) | ((ll8)))

/* 将两个字节数拼成16位无符号整型数,big indian */
#define UI16_MAKE(hi8, lo8)             ((((unint16)(hi8)) <<  8) | ((lo8)))

/* 将两个16位无符号整型数拼成32位无符号整型数,big indian */
#define UI32_MAKE16(h16, l16)           ((((unint32)(h16)) << 16) | ((l16)))

/* 将两个字节数拼成16位无符号整型数再取高12位,big indian */
#define UI16_MAKE_HI12(hi8, lo8)        (((hi8) <<  4) | ((lo8) >> 4))

/* 将两个4位无符号整型数拼成8位无符号整型数,big indian */
#define UI08_MAKE04(h4, l4)             (((h4) << 4) | ((l4)))

/* 设置32位无符号整型数的四个字节数 */
#define UI32_HIHI8_SET(ui08, val)       (((ui08) & 0x00FFFFFF) | ((val) << 24))
#define UI32_HILO8_SET(ui08, val)       (((ui08) & 0xFF00FFFF) | ((val) << 16))
#define UI32_LOHI8_SET(ui08, val)       (((ui08) & 0xFFFF00FF) | ((val) << 8 ))
#define UI32_LOLO8_SET(ui08, val)       (((ui08) & 0xFFFFFF00) | ((val)      ))

/* CONVENIENT_FOR_LOOKUP
void bit_operation(){} */
/* ----------------------------------------------------------------------------------------- */
/* 位操作宏函数 */
/* u08 mask=0x000000FF, bp=0~7 */
/* u16 mask=0x0000FFFF, bp=0~15 */
/* u32 mask=0xFFFFFFFF, bp=0~31 */
#define BIT_GET(uv, bp, mask)           (((uv) >> (bp)) & 0x01)
#define BIT_SET(uv, bp, mask)           ((uv) = (uv) | ((0x01 << (bp)) & (mask)))
#define BIT_CLR(uv, bp, mask)           ((uv) = (uv) & ((0x01 << (bp)) ^ (mask)))

/* u8为字节数据,b8:0~7 */
#define UI08_GET_BIT(uv, bp)            BIT_GET((uv), (bp), MASK_LO08)
#define UI08_SET_BIT(uv, bp)            BIT_SET((uv), (bp), MASK_LO08)
#define UI08_CLR_BIT(uv, bp)            BIT_CLR((uv), (bp), MASK_LO08)

/* u16为字节数据,b16:0~15 */
#define UI16_GET_BIT(uv, bp)            BIT_GET((uv), (bp), MASK_LO16)
#define UI16_SET_BIT(uv, bp)            BIT_SET((uv), (bp), MASK_LO16)
#define UI16_CLR_BIT(uv, bp)            BIT_CLR((uv), (bp), MASK_LO16)

/* u32为字节数据,b32:0~31 */
#define UI32_GET_BIT(uv, bp)            BIT_GET((uv), (bp), MASK_ALL8F)
#define UI32_SET_BIT(uv, bp)            BIT_SET((uv), (bp), MASK_ALL8F)
#define UI32_CLR_BIT(uv, bp)            BIT_CLR((uv), (bp), MASK_ALL8F)

/* CONVENIENT_FOR_LOOKUP
void type_convert(){} */
/* ----------------------------------------------------------------------------------------- */
/* 浮点转换为无符号整型数 */
/* ------------------------------------- */
/* 将32位单精度浮点数转换为32位无符号整型数 */
#define FLT_TO_UN32(x)                  ((unint32)(x))

/* 将32位单精度浮点数转换为16位无符号整型数 */
#define FLT_TO_UN16(x)                  ((unint16)(x))

/* 将32位单精度浮点数转换为8位无符号整型数 */
#define FLT_TO_UN08(x)                  ((unint08)(x))

/* 将64位双精度浮点数转换为64位无符号整型数,会导致错误不能使用 */
#define DBL_TO_UN64(x)                  ((ulong64)(x))

/* 将64位双精度浮点数转换为32位无符号整型数 */
#define DBL_TO_UN32(x)                  ((unint32)(x))

/* 将64位双精度浮点数转换为16位无符号整型数 */
#define DBL_TO_UN16(x)                  ((unint16)(x))

/* 将64位双精度浮点数转换为8位无符号整型数 */
#define DBL_TO_UN08(x)                  ((unint08)(x))

/* 浮点转换为有符号整型数 */
/* ------------------------------------- */
/* 将32位单精度浮点数转换为32位有符号整型数 */
#define FLT_TO_SI32(x)                  ((unint32)((siint32)(x)))

/* 将32位单精度浮点数转换为16位有符号整型数 */
#define FLT_TO_SI16(x)                  ((unint16)((unint32)((siint32)(x))))

/* 将32位单精度浮点数转换为8位有符号整型数 */
#define FLT_TO_SI08(x)                  ((unint08)((unint32)((siint32)(x))))

/* 将64位双精度浮点数转换为64位有符号整型数 */
#define DBL_TO_SI64(x)                  ((ulong64)((llong64)(x)))

/* 将64位双精度浮点数转换为32位有符号整型数 */
#define DBL_TO_SI32(x)                  ((unint32)((siint32)((float32)(x))))

/* 将64位双精度浮点数转换为16位有符号整型数 */
#define DBL_TO_SI16(x)                  ((unint16)((siint32)((float32)(x))))

/* 无符号整型数转换为浮点 */
/* ------------------------------------- */
/* 将32位无符号整型数转换为32位单精度浮点数 */
#define UN32_TO_FLT(x)                  ((float32)(x))

/* 将16位无符号整型数转换为32位单精度浮点数 */
#define UN16_TO_FLT(x)                  ((float32)(x))

/* 将32位无符号整型数强制转换为双精度浮点数 */
#define UN32_TO_DBL(x)                  ((float64)(x))

/* 将16位无符号整型数转换为64位双精度浮点数 */
#define UN16_TO_DBL(x)                  ((float64)(x))

/* 将8位无符号整型数转换为64位双精度浮点数 */
#define UN08_TO_DBL(x)                  ((float64)(x))

/* 有符号整型数转换为浮点 */
/* ------------------------------------- */
/* 将32位有符号整型数转换为32位单精度浮点数 */
#define SI32_TO_FLT(x)                  ((float32)((siint32)(x)))

/* 将16位有符号整型数转换为32位单精度浮点数 */
#define SI16_TO_FLT(x)                  ((float32)((siint16)(x)))

/* 将32位有符号整型数强制转换为双精度浮点数 */
#define SI32_TO_DBL(x)                  ((float64)((siint32)(x)))

/* 将16位有符号整型数转换为64位双精度浮点数 */
#define SI16_TO_DBL(x)                  ((float64)((siint16)(x)))

/* CONVENIENT_FOR_LOOKUP
void express(){} */
/* ----------------------------------------------------------------------------------------- */
#define SIZEOF(v)                       ((unint32)sizeof(v))

/* 判断是否处于全开区间 */
#define IN_RANGE_OPEN(val, lo, up)      (((lo) < (val)) && ((val) < (up)))

/* 判断是否处于全闭区间 */
#define IN_RANGE_CLOSE(val, lo, up)     (((lo) <= (val)) && ((val) <= (up)))

/* 判断是否处于左开右闭区间 */
#define IN_RANGE_LOUC(val, lo, up)      (((lo) < (val)) && ((val) <= (up)))

/* 判断是否处于左闭右开区间 */
#define IN_RANGE_LCUO(val, lo, up)      (((lo) <= (val)) && ((val) < (up)))

#endif /* STD_MACRO_FUN_EXP_H */
