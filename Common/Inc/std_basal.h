/* std_basal.h - 规范化基本数据类型定义头文件 */
/* 版权归属Geek+ */
/* 2015.10.20 */

#ifndef __STD_BASAL_H__
#define __STD_BASAL_H__
#include "stdint.h"
typedef unsigned char  uint8;
typedef unsigned short uint16;
typedef unsigned int   uint32;
typedef unsigned long long   uint64;
typedef          char  int8;
typedef          short int16;
typedef          int   int32;
typedef long long      int64;


typedef  char       		siint08;
typedef unsigned char       unint08;
typedef short               siint16;
typedef unsigned short      unint16;
typedef int                 siint32;
typedef unsigned int        unint32;
typedef float               float32;
typedef double              float64;

typedef volatile char    	volsi08;
typedef volatile char    	volui08;
typedef volatile siint16    volsi16;
typedef volatile unint16    volui16;
typedef volatile siint32    volsi32;
typedef volatile unint32    volui32;
typedef volatile float32    volfl32;
typedef volatile float64    volfl64;

typedef unsigned char       boolu08;    /* 1字节二态布尔型 */
typedef unsigned short      boolu16;    /* 2字节二态布尔型 */
typedef unsigned int        boolu32;    /* 二态布尔型 */
typedef unsigned int        dimorph;    /* 二态型 */
typedef unsigned int        polymor;    /* 多态的 */ 

#ifdef __ARM_COMPILED__
typedef long long           siint64;
typedef unsigned long long  unint64;
#else
typedef __int64             siint64;
typedef unsigned __int64    unint64;
#endif

#define TRUE08              0xAA        /* 逻辑真 */
#define FALSE08             0x00        /* 逻辑假 */

#define TRUE16              0xEB90      /* 逻辑真 */
#define FALSE16             0x0000      /* 逻辑假 */

#define TRUE32              0xEB90146F  /* 逻辑真 */
#define FALSE32             0x00000000  /* 逻辑假 */

/* 健康状态,两态量 */
#define HEALTH              0x00000000  /* 健康 */
#define ILL_HEALTH          0xEB90146F  /* 不健康 */

/* 健康状态,两态量 */
#define VALID               0x00000000  /* 健康 */
#define IN_VALID            0xEB90146F  /* 不健康 */

/* 开关/上断电二态型标志值 */
#define ON                  0x00000000  /* 上电状态 */
#define OFF                 0xEB90146F  /* 断电状态 */

/* 基本库函数重定义 */
#define Sinx            sin
#define Cosx            cos
#define Tanx            tan
#define Atanx           atan
#define Atan2x          atan2
#define Sqrtx           sqrt
#define Ceilx           ceil
#define Floorx          floor
#define Expx            exp
#define Fabsx           fabs
#define Powx            pow
#define Logx            log
#define Log10x          log10

#define Frexpx          frexp       /* 浮点数分解为尾数和指数,frexp(inf32, int *), */
#define Ldexp           ldexp       /* 计算value乘以2的exp次幂ldexp(value, exp) */
#define Modfx           modf        /* 分解浮点数为整数和小数部分 */
#define Fmodx           fmod        /* 求浮点数除法的余数 */

#define Absx            abs         /* 注意:该函数在stdlib.h中 */

/*整型缩写*/
typedef int64_t s64;
typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;

typedef uint64_t u64;
typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

#endif  /* STD_BASAL_H */
