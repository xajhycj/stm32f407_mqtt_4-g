#ifndef __COM_COMMON_H_
#define __COM_COMMON_H_

#include "Common.h"
//#include "InteractWifiDriver.h"
#include "stm32f7xx_it.h"
#include "std_basal.h"


#define CMD_DOWN_MAX_LEN   1080
#define CMD_DOWN_HEADER    0x90EB

typedef enum _stCOM_COMMAND_MAIN_
{
    CMD_MAIN_SYNC = 0x0011,  /*发送系统同步时间                              */
    CMD_MAIN_MOVE = 0x0022,  /*高频(10ms)定时发送电机/灯带控制命令           */
    CMD_MAIN_AUDIO = 0x0033, /*低频(时间间隔不小于100ms)发送声音控制命令     */
    CMD_MAIN_SET = 0x0044,   /*MCU端参数配置和状态设置指令                   */
    CMD_MAIN_GET = 0x0066,   /*读取MCU端参数/文件/状态命令                   */
    CMD_MAIN_OTA = 0x0077,   /*升级主控固件/配置文件/声音文件/子板固件       */
    CMD_MAIN_DBG = 0x00AA,   /*用于调试状态用                                */
} CMDMain;

typedef enum _stCOM_COMMAND_SUB_
{
    CMD_SET_RESET = 0x4804, /*机器人复位                 */
    CMD_SET_REMOTE = 0x4805, /*机器人进入遥控模式         */
    CMD_SET_UPDATE = 0x4806, /*机器人进入程序更新模式     */
    CMD_SET_CALI = 0x4809,     /*机器人进入标定模式         */
    CMD_SET_TEST = 0x480A,     /*机器人进入测试模式         */
    CMD_SET_DISMOTOR = 0x480B, /*所有驱动器禁能             */
    CMD_SET_SLEEPWAKE = 0x480E, /*机器人休眠/唤醒            */
    CMD_SET_SHUTDOWN = 0x480F, /*机器人关机                 */
    CMD_SET_FILE = 0x4810,     /*设置升级文件               */

    CMD_GET_CONFIG = 0x4A01, /*读取配置文件               */
    CMD_GET_FILELIST = 0x4A02, /*读取文件系统列表           */
    CMD_GET_HEALTH = 0x4A03, /*读取健康信息文件           */
    CMD_GET_AUDIO = 0x4A04, /*读取指定声音文件内容       */

    CMD_UPDATE_BMAIN = 0x0070, /*通过Boot固件接收数据，更新主控固件 */
    CMD_UPDATE_MAIN = 0x0071, /*主控软件接收数据，更新主控固件     */
    CMD_UPDATE_BOOT = 0x0072, /*主控固件接收数据，更新Boot固件     */
    CMD_UPDATE_POWER = 0x0077, /*主控升级电源控制板                 */
    CMD_UPDATE_PAUDIO = 0x0078, /*下载声音文件并播放                 */
    CMD_UPDATE_MOTOR = 0x007A, /*主控固件转发电机驱动1-4固件数据    */
    CMD_UPDATE_MOTORCFG = 0x007B, /*主控固件更新电机子系统参数配置文件 */
    CMD_UPDATE_FRONTSUB = 0x007C, /*升级前面板固件                     */
    CMD_UPDATE_BACKSUB = 0x007D, /*升级后面板固件                     */
    CMD_UPDATE_CONFIG = 0x007E, /*主控参数升级                       */
    CMD_UPDATE_AUDIO = 0x007F, /*升级声音文件                       */
} CMDSub;

#pragma pack(push,1)
typedef struct _stCOMMAND_PACKET_
{
    uint16 header;     /* 协议头 */ 
	uint16 length;     /* 协议包长度 */
	uint16 framecnt;   /* 帧序号 */
	uint16 cmdtype;    /* 指令类型 */
	uint8* cmddata;    /* 指令数据 */
	uint16 checksum;   /* 协议校验和 */
	uint16 tailer;     /* 指令结束标识 */
	uint32 update;     /* 新指令标识 */
} stCOMMANDPacket;

typedef struct _stCOMMAND_PARAM_
{
    uint16 cmdmain; /* 主命令字 */
    uint16 cmdsub;  /* 子命令字 */
    uint16 sublen;  /* 数据长度 */
    uint8 cmddata[CMD_DOWN_MAX_LEN];  /* 数据内容 */
}stCOMMANDParam;

typedef struct _stRESPOND_PARAM_
{
    uint16_t cmdsub;  /* 子命令字 */
    uint32_t status;  /* 执行状态 */ 
}stRESPONDParam;

typedef struct _stFILE_PARAM_
{
    uint32_t file_type;   /* 文件类型 */
    uint32_t file_length; /* 文件长度 */
    uint32_t file_checkesum; /* 文件checksum */
    char     file_name[32];  /* 文件名 */
    uint32_t file_update;    /* 文件更新标记 */
}stFILEPARAM;


typedef struct _stRESPOND_OTA_
{
    uint16_t cmdsub;    /* 子命令字 */
    uint32_t packetid;  /* 包序号 */ 
}stRESPONDOTA;

typedef struct _stRESPOND_CONTEXT_
{
    uint16_t cmd_sub;  //读取子命令
    uint32_t file_len; //文件长度
    uint32_t file_checksum; //文件checksum
    uint32_t file_packetsum; //总包数
    uint32_t pakcet_id;      //包序号
    uint8_t  context[1024];  //包内容
}stRESPONDContext;  //反馈文件内容

#pragma pack(pop)

#endif
