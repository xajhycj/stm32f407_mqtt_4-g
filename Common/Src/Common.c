#include "Common.h"
#include "std_basal.h"
#include <math.h>
#include <string.h>
#include "my_task.h"

float Square(float x)
{
	return x*x;
}

/* 8位加法累加和取反 */
u16 CheckSumAdd08Anti(const unsigned char *buffer, int length)
{  
    unint16 addsum08=0;  
    for (int uc=0; uc<length; uc++)
    {
        addsum08 = addsum08 + buffer[uc];   
    }
    addsum08 = ~addsum08;
    return (u16)addsum08;   
}

void delay(u32 t)
{
	while (t--);
}

void delayus(u32 cnt)
{
	for (u32 i = 0; i < cnt; i++) {
		for (u32 j = 0; j < 108; j++)
			__NOP();
	}
}

void delayms(u32 cnt)
{
	if(get_os_start_flag()){
		osDelay(cnt);
	}else{
		HAL_Delay(cnt);
	}
}

u16 CalculateCRC16(const u8 *ptr, u8 len)
{
	u16 crc = 0xFFFF;

	if (len == 0)
		len = 1;
	while (len--)
	{
		crc ^= *ptr;
		for (u8 i = 0; i<8; i++)
		{
			if (crc & 1)
			{
				crc >>= 1;
				crc ^= 0xA001;
			}
			else
				crc >>= 1;
		}
		ptr++;
	}
	return(crc);
}

